package com.example.tpimageleroy;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        registerForContextMenu(findViewById(R.id.LoadedImg));
    }

    private final ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(),
            new ActivityResultCallback<Uri>() {
                @Override
                public void onActivityResult(Uri uri) {
                    if (uri == null) {
                        return;
                    }
                    // Load the image located at uri
                    TextView imagePath = findViewById(R.id.imagePath);
                    imagePath.setText(uri.toString());
                    try {
                        ChargerImage(uri);
                    } catch (FileNotFoundException e) {
                        Toast.makeText(MainActivity.this, "Erreur lors du chargement de l'image", Toast.LENGTH_SHORT).show();
                    }
                }
            });

    private void ChargerImage(Uri imageUri) throws FileNotFoundException {

        BitmapFactory.Options option = new BitmapFactory.Options();
        option.inMutable = true;
        Bitmap bm = BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri), null, option);
        ImageView imageView = findViewById(R.id.LoadedImg);
        imageView.setImageBitmap(bm);
    }

    public void loadImage(View v) {
        mGetContent.launch("image/*");
    }


    public void horizontalFlip(MenuItem item) {
        Bitmap bitmap;
        try {
            bitmap = ((BitmapDrawable) ((ImageView) findViewById(R.id.LoadedImg)).getDrawable()).getBitmap();
        } catch (Exception e) {
            return;
        }
        if (bitmap == null) {
            return;
        }
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
        for (int i = 0; i < bitmap.getWidth(); i++) {
            for (int j = bitmap.getHeight() - 1; j >= 0; j--) {
                newBitmap.setPixel(i, bitmap.getHeight() - j - 1, bitmap.getPixel(i, j));
            }
        }
        ((ImageView) findViewById(R.id.LoadedImg)).setImageBitmap(newBitmap);
    }

    public void verticalFlip(MenuItem item) {
        Bitmap bitmap;
        try {
            bitmap = ((BitmapDrawable) ((ImageView) findViewById(R.id.LoadedImg)).getDrawable()).getBitmap();
        } catch (Exception e) {
            return;
        }
        if (bitmap == null) {
            return;
        }
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
        for (int i = bitmap.getWidth() - 1; i >= 0; i--) {
            for (int j = 0; j < bitmap.getHeight(); j++) {
                newBitmap.setPixel(bitmap.getWidth() - i - 1, j, bitmap.getPixel(i, j));
            }
        }
        ((ImageView) findViewById(R.id.LoadedImg)).setImageBitmap(newBitmap);
    }

    public void inversionCouleur(MenuItem item){
        Bitmap bitmap = ((BitmapDrawable) ((ImageView) findViewById(R.id.LoadedImg)).getDrawable()).getBitmap();
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
        for (int i = 0; i < bitmap.getWidth(); i++){
            for (int j = 0; j < bitmap.getHeight(); j++){
                int pixel = bitmap.getPixel(i, j);
                int red = 255 - android.graphics.Color.red(pixel);
                int green = 255 - android.graphics.Color.green(pixel);
                int blue = 255 - android.graphics.Color.blue(pixel);
                newBitmap.setPixel(i, j, android.graphics.Color.argb(255, red, green, blue));
            }
        }
        ((ImageView) findViewById(R.id.LoadedImg)).setImageBitmap(newBitmap);
    }

        public void transformationGris(MenuItem item){
        Bitmap bitmap = ((BitmapDrawable) ((ImageView) findViewById(R.id.LoadedImg)).getDrawable()).getBitmap();
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
        for (int i = 0; i < bitmap.getWidth(); i++){
            for (int j = 0; j < bitmap.getHeight(); j++){
                int pixel = bitmap.getPixel(i, j);
                int red = android.graphics.Color.red(pixel);
                int green = android.graphics.Color.green(pixel);
                int blue = android.graphics.Color.blue(pixel);
                int moyenne = (red + green + blue) / 3;
                newBitmap.setPixel(i, j, android.graphics.Color.argb(255, moyenne, moyenne, moyenne));
            }
        }
        ((ImageView) findViewById(R.id.LoadedImg)).setImageBitmap(newBitmap);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        if (item.getItemId() == R.id.horizontalFlip) {
            horizontalFlip(item);
            return true;
        }
        if (item.getItemId() == R.id.verticalFlip) {
            verticalFlip(item);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_contextuel, menu);
    }


    public boolean onContextItemSelected(MenuItem item){
        if (item.getItemId() == R.id.inversion_couleur) {
            inversionCouleur(item);
            return true;
        }
        if (item.getItemId() == R.id.transformation_gris) {
            transformationGris(item);
            return true;
        }
        return super.onContextItemSelected(item);
    }

    public void reloadOriginalImage(View v) {
        TextView imagePath = findViewById(R.id.imagePath);
        try {
            ChargerImage(Uri.parse(imagePath.getText().toString()));
        } catch (FileNotFoundException e) {
            Toast.makeText(MainActivity.this, "Impossible d'annuler les modifications d'une image inexistante", Toast.LENGTH_SHORT).show();
        }
    }

    public void rotate90Left(MenuItem item){
        Bitmap bitmap;
        try {
            bitmap = ((BitmapDrawable) ((ImageView) findViewById(R.id.LoadedImg)).getDrawable()).getBitmap();
        } catch (Exception e) {
            return;
        }
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getWidth(), bitmap.getConfig());
        for (int i = 0; i < bitmap.getWidth(); i++){
            for (int j = 0; j < bitmap.getHeight(); j++){
                newBitmap.setPixel(j, bitmap.getWidth() - i - 1, bitmap.getPixel(i, j));
            }
        }
        ((ImageView) findViewById(R.id.LoadedImg)).setImageBitmap(newBitmap);
    }

    public void rotate90Right(MenuItem item){
        Bitmap bitmap;
        try {
            bitmap = ((BitmapDrawable) ((ImageView) findViewById(R.id.LoadedImg)).getDrawable()).getBitmap();
        } catch (Exception e) {
            return;
        }
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getWidth(), bitmap.getConfig());
        for (int i = 0; i < bitmap.getWidth(); i++){
            for (int j = 0; j < bitmap.getHeight(); j++){
                newBitmap.setPixel(bitmap.getHeight() - j - 1, i, bitmap.getPixel(i, j));
            }
        }
        ((ImageView) findViewById(R.id.LoadedImg)).setImageBitmap(newBitmap);
    }

}